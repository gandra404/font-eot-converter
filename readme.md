# Ttf to eot converter tool #

This is a small utility to convert ttf to eot format
Based on [eotlitetool.py](https://github.com/briangonzalez/eotlitetool.py)

## Precondition ##

This tool requires python to run(ver. 2.7).


## Usage ##

#### - To convert one ttf to one eot 

	python ./ttf2eot.py  font.ttf

or eventually use this:

    python ./eotlitetool.py font.ttf && mv font.eotlite font.eot


#### - To convert ttf's from folder to eot's

    python ./ttf2eot.py  --input=folder <path to input folder>




