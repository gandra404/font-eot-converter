#!/usr/bin/env python

import optparse
import struct
import os
from eotlitetool import convert_main
from eotlitetool import convert_ttf
from eotlitetool import FontError


def convert_ttf_folder(argFolder):

    os.chdir(argFolder)
    startPath = os.getcwd()
    print ""
    print "STARTING CONVERSION OF TTF FILES FROM:"
    print startPath
    print ""

    counter = 0
    for root, dirs, files in os.walk("."):
        counter += 1
        path = root.split('/')
        prefixSpace =  (len(path) - 1) *'---'
        debugMsg = ("{0})" + prefixSpace  + " " + os.path.basename(root) + " ").format(counter)
        print debugMsg
        print "--------------------"
        for file in files:
            fileNameOnly, fileExtension = os.path.splitext(file)
            fileExtension = fileExtension.lower()
            if fileExtension == ".ttf":
                prefixFile = len(path)*'   '
                fullFilePath = os.path.join(startPath, os.path.basename(root), file)
                print prefixFile + file
                os.chdir(os.path.join(startPath, os.path.basename(root)))

                try:
                    convert_ttf(file, ".eot", prefixFile)
                except FontError:
                    print prefixFile + "Oops!  That was no valid font!!! " + fullFilePath

                os.chdir(startPath)

def main():

    # deal with options
    p = optparse.OptionParser()
    p.add_option('--input', '-i', default="file")
    p.add_option('--recursive', '-r', default="false")
    p.add_option('--output', '-o', default="world")
    options, args = p.parse_args()

    optionInput = options.input
    optionOutput = options.output



    if(optionInput == 'file'):
        argTtfFile = args[0]
        numArgs = len(args)
        fileName, fileExtension = os.path.splitext(argTtfFile)
        convert_main(".eot")
    else:
        argFolder = args[0]
        convert_ttf_folder(argFolder)


    # iterate over font files
    for f in args:
        print f


if __name__ == '__main__':
    main()